package com.project2;


class Bank{
	int GetRateOfInterestRate(){
		return 0;
	}
}

class SBI extends Bank{
	int GetRateOfInterestRate(){
		return 4;
	}
}

class ICICI extends Bank{
	int GetRateOfInterestRate(){
		return 8;
	}
}

class AXIS extends Bank{
	int GetRateOfInterestRate(){
		return 12;
	}
}

class RunTimePolymorphism{
	public static void main (String args[]){
		Bank b1=new SBI();
		Bank b2=new ICICI();
		Bank b3=new AXIS();
		
		System.out.println(b1.GetRateOfInterestRate());
		System.out.println(b2.GetRateOfInterestRate());
		System.out.println(b3.GetRateOfInterestRate());
		System.out.println("Hello I am in Collection Branch");
	}
}
